
% articulo(clave, descripcion, precio)
producto(pr1, computadora, 15).
producto(pr2, raton_alambrico, 10).
producto(pr3, proyector, 10).
producto(pr4, adaptador_vga, 10).
producto(pr5, raton_inalambrico, 5).
producto(pr6, memoria_usb, 5).
producto(pr7, disco_externo, 5).
producto(pr8, escritorio, 100). 

% inventario(clave, existencias)
inventario(pr1, 32).
inventario(pr2, 4).
inventario(pr3, 5).
inventario(pr4, 64).
inventario(pr5, 12).
inventario(pr6, 10).
inventario(pr7, 89).

% cliente(nombre, ciudad, calificacion-credito)
cliente(ana, fresnillo, 5).
cliente(maria, jerez, 4).
cliente(luis, jerez, 2).
cliente(juan, zacatecas, 3).
cliente(gerardo, gaudalupe, 4).
cliente(david, zacatecas, 5).
cliente(perla, gaudalupe, 4).
cliente(omar, zacatecas, 3).
cliente(lucia, jerez, 4).
cliente(diana, fresnillo, 1).

% _ (guion bajo) COMODIN que permite UNIFICAR cualquier valor, SIN MOSTRARLO

%EJEMPLO: crear regla que muestre a los clientes de Jerez
clientes_jerez(Nombre) :- cliente(Nombre, jerez, _).	%PROBAR CONSULTA: clientes_jerez(Nombre).
/*
	Resultado:
	
	:-clientes_jerez(Nombre).
	true.
	
	Nombre = maria :-;
	Nombre = luis |;
	Nombre = lucia.
*/



%EJEMPLO 2: mostrar los clientes con calificacion de 5
clientes_5(Nombre) :- cliente(Nombre, _, 5).
/*
	Resultado:
	
	:-clientes_5(Nombre).
	true.
	
	Nombre = ana :-;
	Nombre = david.
*/




%EJEMPLO 3: mostrar un listado con el nombre del producto y su existencia
producto_cantidad(Prod, Cant):- producto(NumProd, Prod, _) , inventario(NumProd, Cant).
/*
	Resultado:
	:-producto_cantidad(Prod, Cant).
	true.
	
	Prod = computadora,
	Cant = 32 :-;
	Prod = raton_alambrico,
	Cant = 4 |;
	Prod = proyector,
	Cant = 5 |;
	Prod = adaptador_vga,
	Cant = 64 |;
	Prod = raton_inalambrico,
	Cant = 12 |;
	Prod = memoria_usb,
	Cant = 10 |;
	false.
*/




% --------------------------------------- Reglas -------------------------------------

% 1. Mostrar clientes en una ciudad dada.
clientes_ciudad(Nombre, Ciudad) :- cliente(Nombre, fresnillo, _).

/*
	Resultado:
	true.
	
	Nombre = ana :-;
	Nombre = diana.
*/



% 2. Encontrar clientes con una calificaci�n crediticia determinada.
clientes_calificacion(Nombre, Ciudad) :- cliente(Nombre, Ciudad, 4).
/*
	Resultado:
	:-clientes_calificacion(Nombre, Ciudad).
	true.
	
	Nombre = maria,
	Ciudad = jerez :-;
	Nombre = gerardo,
	Ciudad = gaudalupe |;
	Nombre = perla,
	Ciudad = gaudalupe |;
	Nombre = lucia,
	Ciudad = jerez.
*/



% 3. Encontrar los clientes en una ciudad determinada con una calificaci�n de cr�dito determinada.
clientes_ciudad_calificacion(Nombre, Ciudad, Credito) :- cliente(Nombre, jerez, 4).
/*
	Resultado:
	:-clientes_ciudad_calificacion(Nombre, Ciudad, Credito).
	true.
	
	Nombre = maria :-;
	Nombre = lucia.
*/



% 4. Encuentre la cantidad de limite en existencias para un art�culo dado.
prod_cant_exis(Producto, Cantidad):- producto(Clave, Producto, _) , inventario(Clave, Cantidad).
/*
	Resultado:
	prod_cant_exis(Producto, Cantidad).
	true.
	
	Producto = computadora,
	Cantidad = 32 :-;
	Producto = raton_alambrico,
	Cantidad = 4 |;
	Producto = proyector,
	Cantidad = 5 |;
	Producto = adaptador_vga,
	Cantidad = 64 |;
	Producto = raton_inalambrico,
	Cantidad = 12 |;
	Producto = memoria_usb,
	Cantidad = 10 |;
	false.
*/



% 5. Encuentre el n�mero de art�culo para un nombre de art�culo determinado.
producto_clave(Clave, Producto) :- producto(Clave, computadora, _).
/*
	Resultado:
	:-producto_clave(Clave, Producto).
	true.
	
	Clave = pr1.
*/



% 6. Encuentre el nivel de inventario para un n�mero de art�culo determinado.
inventario_existencia(Clave, Producto, Existencia) :- producto(pr4, Producto, _), inventario(pr4, Existencia).
/*
	Resultado:
	:-inventario_existencia(Clave, Producto, Existencia).
	true.
	
	Producto = adaptador_vga,
	Existencia = 64.
*/



% 7. Crear una regla que ayude a mostrar la cantidad de art�culos que hay para cada art�culo.
cantidad_productos(Clave, Producto, Existencia) :- producto(Clave, Producto, _), inventario(Clave, Existencia).
/*
	Resultado:
	:-cantidad_productos(Clave, Producto, Existencia).
	true.
	
	Clave = pr1,
	Producto = computadora,
	Existencia = 32 :-;
	Clave = pr2,
	Producto = raton_alambrico,
	Existencia = 4 |;
	Clave = pr3,
	Producto = proyector,
	Existencia = 5 |;
	Clave = pr4,
	Producto = adaptador_vga,
	Existencia = 64 |;
	Clave = pr5,
	Producto = raton_inalambrico,
	Existencia = 12 |;
	Clave = pr6,
	Producto = memoria_usb,
	Existencia = 10 |;
	Clave = pr7,
	Producto = disco_externo,
	Existencia = 89 |;
	false.
*/